<?php
// api/src/Entity/Genre.php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * A genre of anime.
 *
 * @ORM\Entity
 *
 * @ApiResource
 */
class Genre {
    /**
     * @var int The id of this Genre.
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string The name of this genre.
     *
     * @ORM\Column
     *
     * @Assert\NotBlank
     */
    private $name;

    /**
     * Get the id of this Genre.
     *
     * @return  int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get the name of this genre.
     *
     * @return  string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set the name of this genre.
     *
     * @param  string  $name  The name of this genre.
     *
     * @return  self
     */
    public function setName(string $name) {
        $this->name = $name;

        return $this;
    }
}
