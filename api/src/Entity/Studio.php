<?php
// api/src/Entity/Studio.php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * A studio producing animes.
 *
 * @ORM\Entity
 *
 * @ApiResource
 */
class Studio {
    /**
     * @var int The id of this studio.
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string The name of this studio.
     *
     * @ORM\Column
     *
     * @Assert\NotBlank
     */
    private $name;

    /**
     * Get the id of this studio.
     *
     * @return  int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get the name of this studio.
     *
     * @return  string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set the name of this studio.
     *
     * @param  string  $name  The name of this studio.
     *
     * @return  self
     */
    public function setName(string $name) {
        $this->name = $name;

        return $this;
    }
}
