<?php
// api/src/Entity/Anime.php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * An anime.
 *
 * @ORM\Entity
 *
 * @ApiResource
 */
class Anime {
    /**
     * @var int The id of this anime.
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string The title of this anime.
     *
     * @ORM\Column
     *
     * @Assert\NotBlank
     */
    private $title;

    /**
     * @var string|null The description of this anime.
     *
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @var \DateTimeInterface The release date of this anime.
     *
     * @ORM\Column(type="datetime")
     */
    private $releaseDate;

    /**
     * @var Review[] Available reviews for this anime.
     *
     * @ORM\OneToMany(targetEntity="Review", mappedBy="anime", cascade={"persist"})
     */
    private $reviews;

    /**
     * @var Manga Manga related to this anime
     *
     * @ORM\ManyToOne(targetEntity="Manga")
     */
    private $manga;

    /**
     * @var int|null Number of episodes for the anime
     *
     * @ORM\Column(type="integer")
     */
    private $episodes;

    /**
     * @var Genre[] Genre(s) of the anime
     *
     * @ORM\ManyToMany(targetEntity="Genre")
     */
    private $genre;

    /**
     * @var Studio Studio producing the anime
     *
     * @ORM\ManyToOne(targetEntity="Studio")
     *
     * @Assert\NotNull
     */
    private $studio;

    public function __construct() {
        $this->reviews = new ArrayCollection();
        $this->genre = new ArrayCollection();
    }

    /**
     * Get the id of this anime.
     *
     * @return  int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get the title of this anime.
     *
     * @return  string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set the title of this anime.
     *
     * @param  string  $title  The title of this anime.
     *
     * @return  self
     */
    public function setTitle(string $title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the description of this anime.
     *
     * @return  string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set the description of this anime.
     *
     * @param  string  $description  The description of this anime.
     *
     * @return  self
     */
    public function setDescription(string $description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the release date of this anime.
     *
     * @return  \DateTimeInterface
     */
    public function getReleaseDate() {
        return $this->releaseDate;
    }

    /**
     * Set the release date of this anime.
     *
     * @param  \DateTimeInterface  $releaseDate  The release date of this anime.
     *
     * @return  self
     */
    public function setReleaseDate(\DateTimeInterface $releaseDate) {
        $this->releaseDate = $releaseDate;

        return $this;
    }

    /**
     * Get available reviews for this anime.
     *
     * @return  Review[]
     */
    public function getReviews() {
        return $this->reviews;
    }

    /**
     * Set available reviews for this anime.
     *
     * @param  Review[]  $reviews  Available reviews for this anime.
     *
     * @return  self
     */
    public function setReviews($reviews) {
        $this->reviews = $reviews;

        return $this;
    }

    /**
     * Add a review to the Review array of the anime
     * Sets the anime for the review added
     *
     * @param Review Review to add to the anime
     */
    public function addReview(Review $review) {
        $review->setAnime($this);
        $this->reviews->add($review);
    }

    /**
     * Removes a review from the reviews linked to this anime
     *
     * @param Review Review to remove from the anime
     */
    public function removeReview(Review $review) {
        $review->removeAnime();
        $this->removeElement($review);
    }

    /**
     * Get manga related to this anime
     *
     * @return  Manga
     */
    public function getManga() {
        return $this->manga;
    }

    /**
     * Set manga related to this anime
     *
     * @param  Manga $manga  Manga related to this anime
     *
     * @return  self
     */
    public function setManga($manga) {
        $this->manga = $manga;

        return $this;
    }

    /**
     * Get number of episodes for the anime
     *
     * @return  int
     */
    public function getEpisodes() {
        return $this->episodes;
    }

    /**
     * Set number of episodes for the anime
     *
     * @param  int  $episodes  Number of episodes for the anime
     *
     * @return  self
     */
    public function setEpisodes(int $episodes) {
        $this->episodes = $episodes;

        return $this;
    }

    /**
     * Get genre(s) of the anime
     *
     * @return  Genre[]
     */
    public function getGenre() {
        return $this->genre;
    }

    /**
     * Set genre(s) of the anime
     *
     * @param  Genre[]  $genre  Genre(s) of the anime
     *
     * @return  self
     */
    public function setGenre($genre) {
        $this->genre = $genre;

        return $this;
    }

    /**
     * Get studio producing the anime
     *
     * @return  Studio
     */
    public function getStudio() {
        return $this->studio;
    }

    /**
     * Set studio producing the anime
     *
     * @param  Studio  $studio  Studio producing the anime
     *
     * @return  self
     */
    public function setStudio(Studio $studio) {
        $this->studio = $studio;

        return $this;
    }
}
