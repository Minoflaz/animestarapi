<?php
// api/src/Entity/Review.php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * A review of an anime.
 *
 * @ORM\Entity
 *
 * @ApiResource
 */
class Review {
    /**
     * @var int The id of this Review.
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var int The rating of this review (between 0 and 5).
     *
     * @ORM\Column(type="smallint")
     *
     * @Assert\Range(min=0, max=5)
     */
    public $stars;

    /**
     * @var string the body of the review.
     *
     * @ORM\Column(type="text")
     *
     * @Assert\NotBlank
     */
    public $body;

    /**
     * @var string The author of the review.
     *
     * @ORM\Column
     *
     * @Assert\NotBlank
     */
    public $author;

    /**
     * @var \DateTimeInterface The date of publication of this review.
     *
     * @ORM\Column(type="datetime_immutable")
     *
     * @Assert\NotNull
     */
    public $publicationDate;

    /**
     * @var Anime The anime this review is about.
     *
     * @ORM\ManyToOne(targetEntity="Anime", inversedBy="reviews")
     *
     * @Assert\NotNull
     */
    public $anime;

    /**
     * Get the id of this Review.
     *
     * @return  int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get the rating of this review (between 0 and 5).
     *
     * @return  int
     */
    public function getStars() {
        return $this->stars;
    }

    /**
     * Set the rating of this review (between 0 and 5).
     *
     * @param  int  $stars  The rating of this review (between 0 and 5).
     *
     * @return  self
     */
    public function setStars(int $stars) {
        $this->stars = $stars;

        return $this;
    }

    /**
     * Get the body of the review.
     *
     * @return  string
     */
    public function getBody() {
        return $this->body;
    }

    /**
     * Set the body of the review.
     *
     * @param  string  $body  the body of the review.
     *
     * @return  self
     */
    public function setBody(string $body) {
        $this->body = $body;

        return $this;
    }

    /**
     * Get the author of the review.
     *
     * @return  string
     */
    public function getAuthor() {
        return $this->author;
    }

    /**
     * Set the author of the review.
     *
     * @param  string  $author  The author of the review.
     *
     * @return  self
     */
    public function setAuthor(string $author) {
        $this->author = $author;

        return $this;
    }

    /**
     * Get the date of publication of this review.
     *
     * @return  \DateTimeInterface
     */
    public function getPublicationDate() {
        return $this->publicationDate;
    }

    /**
     * Set the date of publication of this review.
     *
     * @param  \DateTimeInterface  $publicationDate  The date of publication of this review.
     *
     * @return  self
     */
    public function setPublicationDate(\DateTimeInterface $publicationDate) {
        $this->publicationDate = $publicationDate;

        return $this;
    }

    /**
     * Get the anime this review is about.
     *
     * @return  Anime
     */
    public function getAnime() {
        return $this->anime;
    }

    /**
     * Set the anime this review is about.
     *
     * @param  Anime  $anime  The anime this review is about.
     *
     * @return  self
     */
    public function setAnime(Anime $anime) {
        $this->anime = $anime;

        return $this;
    }

    /**
     * Removes the anime linked to this review
     */
    public function removeAnime() {
        $this->anime = null;
    }
}
