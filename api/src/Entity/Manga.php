<?php
// api/src/Entity/Manga.php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * A manga.
 *
 * @ORM\Entity
 *
 * @ApiResource
 */
class Manga {
    /**
     * @var int The id of this Manga.
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string The title of this manga.
     *
     * @ORM\Column
     *
     * @Assert\NotBlank
     */
    private $title;

    /**
     * @var string|null The description of this manga.
     *
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @var \DateTimeInterface The release date of this manga.
     *
     * @ORM\Column(type="datetime")
     *
     * @Assert\NotNull
     */
    private $releaseDate;

    /**
     * @var string The author of the review.
     *
     * @ORM\Column
     *
     * @Assert\NotBlank
     */
    private $author;

    /**
     * Get the id of this Manga.
     *
     * @return  int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Get the title of this manga.
     *
     * @return  string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set the title of this manga.
     *
     * @param  string  $title  The title of this manga.
     *
     * @return  self
     */
    public function setTitle(string $title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the description of this manga.
     *
     * @return  string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set the description of this manga.
     *
     * @param  string  $description  The description of this manga.
     *
     * @return  self
     */
    public function setDescription(string $description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get the release date of this manga.
     *
     * @return  \DateTimeInterface
     */
    public function getReleaseDate() {
        return $this->releaseDate;
    }

    /**
     * Set the release date of this manga.
     *
     * @param  \DateTimeInterface  $releaseDate  The release date of this manga.
     *
     * @return  self
     */
    public function setReleaseDate(\DateTimeInterface $releaseDate) {
        $this->releaseDate = $releaseDate;

        return $this;
    }

    /**
     * Get the author of the review.
     *
     * @return  string
     */
    public function getAuthor() {
        return $this->author;
    }

    /**
     * Set the author of the review.
     *
     * @param  string  $author  The author of the review.
     *
     * @return  self
     */
    public function setAuthor(string $author) {
        $this->author = $author;

        return $this;
    }
}
